package ru.t1.chubarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;

import java.io.IOException;

public interface IListener {

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException, IOException;

}
