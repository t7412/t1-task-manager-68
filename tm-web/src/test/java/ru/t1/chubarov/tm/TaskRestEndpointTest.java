package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.client.TaskRestEndpointClient;
import ru.t1.chubarov.tm.marker.IntegrationCategory;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final Task task_1 = new Task("TestTask_1","Desc_Task_1");

    @NotNull
    final Task task_2 = new Task("TestTask_2","Desc_Task_2");

    @NotNull
    final Task task_3 = new Task("TestTask_3","Desc_Task_3");

    @Before
    public void initTest() {
        client.add(task_1);
        client.add(task_2);
    }

    @After
    public void finish() {
        client.delete(task_1.getId());
        client.delete(task_2.getId());
        client.delete(task_3.getId());
    }

    @Test
    public void testFindAll() {
        @Nullable final Collection<Task> tasks = client.findAll();
        Assert.assertEquals(tasks.size(), 2);
    }

    @Test
    public void testCreate() {
        //Assert.assertThrows(EntityNotFoundException.class, () -> client.create());
        @Nullable final Task task = client.create();
        Assert.assertNotNull(task);
        client.delete(task.getId());
    }

    @Test
    public void testAdd() {
        @Nullable final Task task = client.add(task_3);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), "TestTask_3");
    }

    @Test
    public void testFindById() {
        @Nullable final Task task = client.findById(task_1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task_1.getId());
    }

    @Test
    public void testDelete() {
        client.delete(task_2.getId());
        Assert.assertNull(client.findById(task_2.getId()));
    }

    @Test
    public void testSave() {
        @Nullable final Task task = new Task("Test_Save+Task","Desc_Task_Test");
        client.save(task);
        @Nullable final Task task_check = client.findById(task.getId());
        Assert.assertNotNull(task_check);
        Assert.assertEquals(task_check.getId(), task.getId());
        Assert.assertEquals(task_check.getName(), "Test_Save+Task");
        client.delete(task.getId());
    }

}
