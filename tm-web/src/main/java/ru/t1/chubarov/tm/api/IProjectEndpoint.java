package ru.t1.chubarov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.chubarov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @WebMethod
    @PostMapping("/create")
    Project create();

    @WebMethod
    @PostMapping("/add")
    Project add(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project);

    @WebMethod
    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

}
