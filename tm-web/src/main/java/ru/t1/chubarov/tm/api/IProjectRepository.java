package ru.t1.chubarov.tm.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.Project;

@Repository
public interface IProjectRepository extends JpaRepository<Project,String> {

}
