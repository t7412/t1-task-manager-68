package ru.t1.chubarov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.chubarov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @PostMapping("/create")
    Task create();

    @WebMethod
    @PostMapping("/add")
    Task add(@RequestBody Task task);

    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @WebMethod
    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

}
