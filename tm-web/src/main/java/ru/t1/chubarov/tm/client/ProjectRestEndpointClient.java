package ru.t1.chubarov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.chubarov.tm.model.Project;

import java.util.Collection;

public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/projects/";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("findAll")
    Collection<Project> findAll();

    @PostMapping("create")
    Project create();

    @PostMapping(value = "add")
    Project add(@RequestBody Project project);

    @PostMapping("save")
    Project save(@RequestBody Project project);

    @GetMapping("findById/{id}")
    Project findById(@PathVariable("id") String id);

    @PostMapping(value = "delete/{id}")
    void delete(@PathVariable("id") String id);

}
