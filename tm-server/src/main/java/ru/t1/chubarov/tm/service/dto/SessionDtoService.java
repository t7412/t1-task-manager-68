package ru.t1.chubarov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.chubarov.tm.api.service.dto.ISessionDtoService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.SessionNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public SessionDTO findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO model = repository.findFirstByIdAndUserId(userId, id);
        if (model == null) throw new SessionNotFoundException();
        return model;
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO remove(@NotNull final String userId, @Nullable SessionDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.deleteByUserIdAndId(userId, model.getId());
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@NotNull final String userId, @Nullable final Collection<SessionDTO> models) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        for (@NotNull final SessionDTO session : models) {
            repository.deleteByUserIdAndId(userId, session.getId());
        }
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO model = new SessionDTO();
        model.setId(id);
        repository.deleteByUserIdAndId(userId, id);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO removeOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO model = new SessionDTO();
        model.setId(id);
        repository.deleteById(id);
        return model;
    }

    @Override
    public long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countAllByUserId(userId);
    }

    @Override
    public long getSize() throws Exception {
        return repository.count();
    }

}
