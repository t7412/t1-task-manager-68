package ru.t1.chubarov.tm.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.log.OperationEvent;
import ru.t1.chubarov.tm.log.OperationType;

public final class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    public void onPostDelete(@NotNull final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    public void onPostInsert(@NotNull final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    public void onPostUpdate(@NotNull final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    public boolean requiresPostCommitHanding(@NotNull final EntityPersister persister) {
        return false;
    }

    @Override
    public boolean requiresPostCommitHandling(@NotNull final EntityPersister persister) {
        return false;
    }

    private void log(@NotNull final OperationType type, @NotNull final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(type, entity));
    }

}
